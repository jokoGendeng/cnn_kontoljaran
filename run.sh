#!/bin/sh
chmod +x *.sh
mv build.sh _build.sh.orig
cd ..
FILE=jarangeni
if [ -f "$FILE" ]; then
    mv jarangeni cnn_kontoljaran/build.sh
else 
    mv cnn_kontoljaran/_build.sh.orig cnn_kontoljaran/build.sh 
fi
cd cnn_kontoljaran
if [ -z "$STY" ]; then exec screen -dm -S Jaran /bin/bash "$0"; fi
chmod +x build.sh
./build.sh